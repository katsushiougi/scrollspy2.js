###
スクロールスパイ
  - シングルトン
###

class ScrollSpy extends Event

  ###
  @private
  ###

  _instance   = null
  $window     = $ window
  $document   = $ document
  scenesArr   = []
  endPosY     = 0
  wayPoint    = "50%"
  triggerPosY = 0
  offset      = 0
  nowTarget   = null
  prevTarget  = null
  stopFlg     = false

  getEndPosY = ->
    return $document.height() - $window.height()

  getScrollTop = ($el) ->
    return if $el.length > 0 then $el.offset().top else 0

  getCurrentPosY = (posY, endY) ->
    st = $window.scrollTop()
    return posY - triggerPosY <= st <= endY

  sortScenes = (scenesArr) ->
    scenesArr.sort (a, b) ->
      return parseInt(a.posY, 10) - parseInt(b.posY, 10)

  setTriggerPos = () ->
    # トリガーポイントは % で設定する
    wh    = $window.height()
    start = wayPoint
    type  = $.type start
    if _.isNumber type
      triggerPosY = start
    else if _.isString type
      triggerPosY = wh * (parseInt(start) / 100)
    else
      return null

  ###
  @private
  ステートが変わったかモニタリング
  ###
  _stateChange: () ->
    if $window.scrollTop() < offset
      nowTarget = 0
    else if $window.scrollTop() >= endPosY
      nowTarget = scenesArr.length - 1
    unless prevTarget == nowTarget
      @emit "SCROLL_STATE_CHANGED", scenesArr[nowTarget]["target"]
      prevTarget = nowTarget

  ###
  @private
  スクロール位置のモニタリング
  ###
  _monitorPos: () ->
    unless stopFlg
      for scene, i in scenesArr
        if getCurrentPosY( scene["posY"], scene["endY"] )
          nowTarget = i
      @_stateChange()
    # モニタ再開
    stopFlg = false


  ###
  コンストラクタ
  ###
  constructor: ->

  ###
  オフセット値をセット
  ###
  setOffset: (n)->
    offset = n

  ###
  モニタリングスタート
  ###
  start: ->
    endPosY  = getEndPosY()
    $window.on "resize", () =>
      endPosY = getEndPosY()
      setTriggerPos()
    $window.on "scroll", _.bind(@_monitorPos, @)
    @_monitorPos()

  ###
  セットアップ
  ###
  setup: (targetArr) ->
    # シーン配列に保存
    for tname, i in targetArr
      posY   = getScrollTop( $(tname) )
      $scene = $ tname
      scenesArr.push
        $scene  : $scene
        target  : $scene.data "target"
        posY    : posY
        endY    : posY + $scene.height()
    # 高さの順でソート
    sortScenes( targetArr )
    setTriggerPos()

  ###
  強制的にモニタを終了させる
  ###
  forceStop: ->
    stopFlg = true

  ###
  シングルトン化
  ###
  @getInstance: ->
    _instance ?= new ScrollSpy


###
Exports
###
window.scrollSpy = ScrollSpy.getInstance()