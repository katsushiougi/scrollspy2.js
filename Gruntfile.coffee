pkg = require './package.json'

module.exports = (grunt) ->

  for taskName of pkg.devDependencies
    if taskName.substring(0, 6) == 'grunt-' then grunt.loadNpmTasks taskName

  config =
    pkg: grunt.file.readJSON('package.json')

    ###
    grunt-contrib-coffee
    ###
    coffee:
      dist:
        options:
          join: true
        files:
          'scrollspy.js': [
            'libs/event.coffee'
            'scrollspy.coffee'
          ]

    ###
    grunt-contrib-uglify
    @ 圧縮
    ###
    uglify:
      dist:
        files:
          './scrollspy.min.js': ['scrollspy.js']

    ###
    grunt-notify
    @ ビルド通知
    ###
    notify:
      build:
        options:
          title  : 'ビルド完了'
          message: 'タスクが正常終了しました。'
      watch:
        options:
          title  : '監視開始'
          message: 'ローカルサーバーを起動しました'

    ###
    grunt-contrib-connect
    @ ローカルサーバー
    ###
    connect:
      server:
        options:
          port    : 3000
          base    : './'
          hostname: '0.0.0.0'
          spawn   : false

    ###
    grunt-contrib-watch
    @ 更新監視
    ###
    watch:
      options:
        livereload: true
        spawn     : false
      js:
        files: ['./**/*.coffee']
        tasks: [
          'coffee'
          'notify:build'
        ]

  grunt.initConfig( config )

  grunt.registerTask 'default', []

  # Start Task
  grunt.registerTask 'start', [
    'coffee'
    'uglify'
    'notify:watch'
    'connect'
    'watch'
  ]

  # Build Task
  grunt.registerTask 'build', [
    'coffee'
    'uglify'
    'notify:build'
  ]